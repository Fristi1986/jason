package jason

import jason.JsonF.Json
import matryoshka.data.Fix

import scalaz._
import scalaz.Scalaz._

trait SchemaExtractors {
  protected object BooleanExtractor {
    def unapply(map: Map[String, Json]): Option[Unit] = map.get("type").filter(_ == Fix[JsonF](JsonF.Str("boolean"))).map(_ => ())
  }

  protected object NullExtractor {
    def unapply(map: Map[String, Json]): Option[Unit] = map.get("type").filter(_ == Fix[JsonF](JsonF.Str("null"))).map(_ => ())
  }

  protected object CollectionExtractor {
    def unapply(map: Map[String, Json]): Option[(Json, Boolean, LengthBound)] = {
      map.get("type").filter(_ == Fix[JsonF](JsonF.Str("array"))).flatMap { _ =>
        for {
          items <- map.get("items")
          uniqueItems <- map.get("uniqueItems").collect { case Fix(JsonF.Bool(value)) => value }
          lowerBound = map.get("minItems").collect { case Fix(JsonF.Number(value)) => value.toInt }
          upperBound = map.get("maxItems").collect { case Fix(JsonF.Number(value)) => value.toInt }
          lengthBound = (lowerBound, upperBound) match {
            case (Some(low), Some(high)) => LengthBound.Interval(low, high)
            case (None, Some(high)) => LengthBound.Atmost(high)
            case (Some(low), None) => LengthBound.Atleast(low)
            case (None, None) => LengthBound.Unbounded
          }
        } yield (items, uniqueItems, lengthBound)
      }
    }
  }

  protected object IntExtractor {
    def unapply(map: Map[String, Json]): Option[(Long, Boolean, Long, Boolean, IntegerType)] = {
      map.get("type").filter(_ == Fix[JsonF](JsonF.Str("integer"))).flatMap { _ =>
        for {
          minimum <- map.get("minimum").collect { case Fix(JsonF.Number(value)) => value.toLong }
          exclusiveMinimum <- map.get("exclusiveMinimum").collect { case Fix(JsonF.Bool(value)) => value }
          maximum <- map.get("maximum").collect { case Fix(JsonF.Number(value)) => value.toLong }
          exclusiveMaximum <- map.get("exclusiveMaximum").collect { case Fix(JsonF.Bool(value)) => value }
          integerType <- map.get("format").collect { case Fix(JsonF.Str(format)) => format } >>= IntegerType.fromString
        } yield (minimum, exclusiveMinimum, maximum, exclusiveMaximum, integerType)
      }
    }
  }

  protected object NumberExtractor {
    def unapply(map: Map[String, Json]): Option[(Long, Boolean, Long, Boolean, NumberType)] = {
      map.get("type").filter(_ == Fix[JsonF](JsonF.Str("number"))).flatMap { _ =>
        for {
          minimum <- map.get("minimum").collect { case Fix(JsonF.Number(value)) => value.toLong }
          exclusiveMinimum <- map.get("exclusiveMinimum").collect { case Fix(JsonF.Bool(value)) => value }
          maximum <- map.get("maximum").collect { case Fix(JsonF.Number(value)) => value.toLong }
          exclusiveMaximum <- map.get("exclusiveMaximum").collect { case Fix(JsonF.Bool(value)) => value }
          numberType <- map.get("format").collect { case Fix(JsonF.Str(format)) => format } >>= NumberType.fromString
        } yield (minimum, exclusiveMinimum, maximum, exclusiveMaximum, numberType)
      }
    }
  }

  protected object StrFormatExtractor {
    def unapply(map: Map[String, Json]): Option[StringType] = {
      map.get("type").filter(_ == Fix[JsonF](JsonF.Str("string"))).flatMap { _ =>
        for {
          format <- map.get("format").collect { case Fix(JsonF.Str(value)) => value }
          t <- StringType.fromString(format)
        } yield t
      }
    }
  }

  protected object StrPatternExtractor {
    def unapply(map: Map[String, Json]): Option[String] = {
      map.get("type").filter(_ == Fix[JsonF](JsonF.Str("string"))).flatMap { _ =>
        for {
          pattern <- map.get("pattern").collect { case Fix(JsonF.Str(value)) => value }
        } yield pattern
      }
    }
  }

  protected object StrBoundedLengthExtractor {
    def unapply(map: Map[String, Json]): Option[LengthBound] = {
      map.get("type").filter(_ == Fix[JsonF](JsonF.Str("string"))).map { _ =>
        val minLength = map.get("minLength").collect { case Fix(JsonF.Number(value)) => value.toInt }
        val maxLength = map.get("maxLength").collect { case Fix(JsonF.Number(value)) => value.toInt }

        (minLength, maxLength) match {
          case (Some(low), Some(high)) => LengthBound.Interval(low, high)
          case (None, Some(high)) => LengthBound.Atmost(high)
          case (Some(low), None) => LengthBound.Atleast(low)
          case (None, None) => LengthBound.Unbounded
        }
      }
    }
  }

  protected object StrUnboundedLengthExtractor {
    def unapply(map: Map[String, Json]): Option[Unit] = {
      map.get("type").filter(_ == Fix[JsonF](JsonF.Str("string"))).map { _ => () }
    }
  }

  protected object RefExtractor {
    def unapply(map: Map[String, Json]): Option[String] = {
      map.get("$ref").collect {
        case Fix(JsonF.Str(value)) if value.startsWith("#/definitions/") =>
          value.substring("#/definitions/".length)
      }
    }
  }

  protected object EnumExtractor {
    def unapply(map: Map[String, Json]): Option[Set[String]] = {
      map.get("type")
        .collect { case Fix(JsonF.Object(fields)) => fields }
        .flatMap(_.get("enum"))
        .collect { case Fix(JsonF.Array(items)) => items }
        .map(_.collect { case Fix(JsonF.Str(value)) => value })
        .map(_.toSet)
    }
  }

  protected object ChoiceExtractor {
    def unapply(map: Map[String, Json]): Option[NonEmptyList[JsonF.Json]] = {
      map.get("oneOf").collect {
        case Fix(JsonF.Array(values)) if values.nonEmpty => NonEmptyList(values.head, values.tail: _*)
      }
    }
  }
}
