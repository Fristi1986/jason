package jason

import java.time._
import java.time.format.DateTimeFormatter
import java.util.UUID

import shapeless.{:+:, CNil, Coproduct, Inl, Inr}

import scala.util.Try
import scalaz.InvariantFunctor

trait Jason[A] {
  def apply[F[_] : JasonAlgebra]: F[A]
}

object Jason {
  def applyAlgebra[F[_], A](avro: Jason[A])(implicit F: JasonAlgebra[F]): F[A] = avro.apply[F]
}

trait JasonDsl extends JasonDslFormatN { self =>

  val int: Jason[Int] = new Jason[Int] {
    override def apply[F[_] : JasonAlgebra]: F[Int] = implicitly[JasonAlgebra[F]].int(Range(Bound(Int.MinValue, true), Bound(Int.MaxValue, true)))
  }

  val string: Jason[String] = new Jason[String] {
    override def apply[F[_] : JasonAlgebra]: F[String] = implicitly[JasonAlgebra[F]].string(StringDescription.Unbounded)
  }

  val bool: Jason[Boolean] = new Jason[Boolean] {
    override def apply[F[_] : JasonAlgebra]: F[Boolean] = implicitly[JasonAlgebra[F]].bool
  }

  val long: Jason[Long] = new Jason[Long] {
    override def apply[F[_] : JasonAlgebra]: F[Long] = implicitly[JasonAlgebra[F]].long(Range(Bound(Long.MinValue, true), Bound(Long.MaxValue, true)))
  }

  val double: Jason[Double] = new Jason[Double] {
    override def apply[F[_] : JasonAlgebra]: F[Double] = implicitly[JasonAlgebra[F]].double(Range(Bound(Double.MinValue, true), Bound(Double.MaxValue, true)))
  }

  val float: Jason[Float] = new Jason[Float] {
    override def apply[F[_] : JasonAlgebra]: F[Float] = implicitly[JasonAlgebra[F]].float(Range(Bound(BigDecimal(Float.MinValue), true), Bound(BigDecimal(Float.MaxValue), true)))
  }

  val cnil: Jason[CNil] = new Jason[CNil] {
    override def apply[F[_] : JasonAlgebra]: F[CNil] = implicitly[JasonAlgebra[F]].cnil
  }

  val uuid: Jason[UUID] = new Jason[UUID] {
    override def apply[F[_] : JasonAlgebra]: F[UUID] = implicitly[JasonAlgebra[F]].uuid
  }

  val instant: Jason[Instant] = new Jason[Instant] {
    override def apply[F[_] : JasonAlgebra]: F[Instant] = implicitly[JasonAlgebra[F]].instant
  }

  //TODO: regex
  val localDate: Jason[LocalDate] =
    string.pmap(str => Attempt.fromTry(Try(LocalDate.parse(str, DateTimeFormatter.ISO_LOCAL_DATE))))(_.format(DateTimeFormatter.ISO_LOCAL_DATE))

  //TODO: regex
  val localDateTime: Jason[LocalDateTime] =
    string.pmap(str => Attempt.fromTry(Try(LocalDateTime.parse(str, DateTimeFormatter.ISO_LOCAL_DATE_TIME))))(_.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME))

  //TODO: regex
  val zonedDateTime: Jason[ZonedDateTime] =
    string.pmap(str => Attempt.fromTry(Try(ZonedDateTime.parse(str, DateTimeFormatter.ISO_ZONED_DATE_TIME))))(_.format(DateTimeFormatter.ISO_ZONED_DATE_TIME))

  def imap[A, B](fa: Jason[A])(f: A => B)(g: B => A): Jason[B] = new Jason[B] {
    override def apply[F[_] : JasonAlgebra]: F[B] = implicitly[JasonAlgebra[F]].imap(fa.apply[F])(f)(g)
  }

  def pmap[A, B](fa: Jason[A])(f: A => Attempt[B])(g: B => A): Jason[B] = new Jason[B] {
    override def apply[F[_] : JasonAlgebra]: F[B] = implicitly[JasonAlgebra[F]].pmap(fa.apply[F])(f)(g)
  }

  def option[A](value: Jason[A]): Jason[Option[A]] = new Jason[Option[A]] {
    override def apply[F[_] : JasonAlgebra]: F[Option[A]] = implicitly[JasonAlgebra[F]].option(value.apply[F])
  }

  def list[A](of: Jason[A]): Jason[List[A]] = new Jason[List[A]] {
    override def apply[F[_] : JasonAlgebra]: F[List[A]] = implicitly[JasonAlgebra[F]].list(of.apply[F])
  }

  def set[A](of: Jason[A]): Jason[Set[A]] = new Jason[Set[A]] {
    override def apply[F[_] : JasonAlgebra]: F[Set[A]] = implicitly[JasonAlgebra[F]].set(of.apply[F])
  }

  def vector[A](of: Jason[A]): Jason[Vector[A]] = new Jason[Vector[A]] {
    override def apply[F[_] : JasonAlgebra]: F[Vector[A]] = implicitly[JasonAlgebra[F]].vector(of.apply[F])
  }

  def seq[A](of: Jason[A]): Jason[Seq[A]] = new Jason[Seq[A]] {
    override def apply[F[_] : JasonAlgebra]: F[Seq[A]] = implicitly[JasonAlgebra[F]].seq(of.apply[F])
  }

  def or[A, B](fa: Jason[A], fb: Jason[B]): Jason[Either[A, B]] = new Jason[Either[A, B]] {
    override def apply[F[_] : JasonAlgebra]: F[Either[A, B]] = implicitly[JasonAlgebra[F]].or(fa.apply[F], fb.apply[F])
  }

  implicit class RichJason[A](val fa: Jason[A]) {
    def imap[B](f: A => B)(g: B => A): Jason[B] = self.imap(fa)(f)(g)
    def pmap[B](f: A => Attempt[B])(g: B => A): Jason[B] = self.pmap(fa)(f)(g)
    def |[B](fb: Jason[B]): UnionBuilder[A :+: B :+: CNil] =
      new UnionBuilder[CNil](cnil).add(fb).add(fa)
  }

  implicit val invariantFunctor: InvariantFunctor[Jason] = new InvariantFunctor[Jason] {
    override def xmap[A, B](fa: Jason[A], f: A => B, g: B => A): Jason[B] = self.imap(fa)(f)(g)
  }

  final class UnionBuilder[B <: Coproduct](fb: Jason[B]) {

    def add[A](fa: Jason[A]): UnionBuilder[A :+: B] = {
      val coproduct = imap(or(fa, fb)) {
        case Left(l) => Inl(l)
        case Right(r) => Inr(r)
      } {
        case Inl(l) => Left(l)
        case Inr(r) => Right(r)
      }

      new UnionBuilder(coproduct)
    }

    def |[A](fa: Jason[A]): UnionBuilder[A :+: B] = add(fa)

    def as[A](implicit T: Transformer[Jason, B, A]): Jason[A] = T(fb)
  }
}