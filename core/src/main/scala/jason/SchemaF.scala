package jason

import matryoshka._
import matryoshka.implicits._
import matryoshka.patterns._
import matryoshka.data._
import JsonF._

import scalaz.{Equal, _}
import Scalaz._

sealed trait SchemaF[+A]

object SchemaF extends SchemaExtractors {
  final case class Choice[A](choices: NonEmptyList[A]) extends SchemaF[A]
  final case class Opt[A](schema: A) extends SchemaF[A]
  final case class Object[A](name: String, fields: Map[String, A]) extends SchemaF[A]
  final case class Collection[A](lengthBound: LengthBound, uniqueItems: Boolean, schema: A) extends SchemaF[A]
  final case class Documented[A](documentation: String, schema: A) extends SchemaF[A]
  final case class Ref(id: String) extends SchemaF[Nothing]
  final case class Enum(choices: Set[String]) extends SchemaF[Nothing]
  final case class Str(description: StringDescription) extends SchemaF[Nothing]
  final case class Number(range: Range, numberType: NumberType) extends SchemaF[Nothing]
  final case class Integer(range: Range, integerType: IntegerType) extends SchemaF[Nothing]
  case object Boolean extends SchemaF[Nothing]
  case object Null extends SchemaF[Nothing]

  implicit val traverse: Traverse[SchemaF] = new Traverse[SchemaF] {
    override def traverseImpl[G[_], A, B](fa: SchemaF[A])(f: A => G[B])(implicit G: Applicative[G]): G[SchemaF[B]] =
      fa match {
        case Choice(choices) => choices.traverse(f).map(Choice.apply)
        case Opt(schema) => f(schema).map(Opt.apply)
        case Documented(doc, schema) => f(schema).map(Documented(doc, _))
        case Object(name, fields) => Traverse[Map[String, ?]].traverse(fields)(f).map(Object(name, _))
        case Collection(bound, uniqueItems, schema) => f(schema).map(x => Collection(bound, uniqueItems, x))
        case Ref(name) => G.pure(Ref(name))
        case Enum(choices) => G.pure(Enum(choices))
        case Str(description) => G.pure(Str(description))
        case Number(bound, t) => G.pure(Number(bound, t))
        case Integer(bound, t) => G.pure(Integer(bound, t))
        case Boolean => G.pure(Boolean)
        case Null => G.pure(Null)
      }
  }

  implicit val equal: Delay[Equal, SchemaF] = new Delay[Equal, SchemaF] {
    override def apply[A](eq: Equal[A]): Equal[SchemaF[A]] = Equal.equal((a, b) => {
      implicit val ieq = eq
      (a, b) match {
        case (Null, Null) => true
        case (Boolean, Boolean) => true
        case (Choice(l), Choice(r)) => l == r
        case (Opt(l), Opt(r)) => l === r
        case (Object(namel, l), Object(namer, r)) => namel == namer && l == r
        case (Collection(lb, lui, l), Collection(rb, rui, r)) => lb == rb && lui === rui && l === r
        case (Ref(l), Ref(r)) => l === r
        case (Enum(l), Enum(r)) => l === r
        case (Str(l), Str(r)) => l == r
        case (Number(lb, l), Number(rb, r)) => lb == rb && l == r
        case (Integer(lb, l), Integer(rb, r)) => lb == rb && l == r
        case _ => false
      }
    })
  }

  implicit val merge: Merge[SchemaF] = Merge.fromTraverse[SchemaF]

  implicit val diffable: Diffable[SchemaF] = new Diffable[SchemaF] {
    override def diffImpl[T[_[_]]](ls: T[SchemaF], rs: T[SchemaF])(implicit BT: BirecursiveT[T]): Option[DiffT[T, SchemaF]] = (ls.project, rs.project) match {
      case (l@Null, r@Null) => localDiff(l, r).some
      case (l@Boolean, r@Boolean) => localDiff(l, r).some
      case (l: Integer, r: Integer) => localDiff(l, r).some
      case (l: Number, r: Number) => localDiff(l, r).some
      case (l: Str, r: Str) => localDiff(l, r).some
      case (l: Enum, r: Enum) => localDiff(l, r).some
      case (l: Ref, r: Ref) => localDiff(l, r).some
      case (l@Collection(_, _, _), r@Collection(_, _, _)) => localDiff(l, r).some
      case (l@Opt(_), r@Opt(_)) => localDiff(l, r).some
      case (Object(nl, l), Object(nr, r)) =>
        if (nl === nr && l.keySet == r.keySet) {
          Similar[T, SchemaF, T[Diff[T, SchemaF, ?]]](Object[DiffT[T, SchemaF]](nl, diffTraverse(l, r))).embed.some
        } else {
          Different[T, SchemaF, T[Diff[T, SchemaF, ?]]](ls, rs).embed.some
        }
      case (Choice(l), Choice(r)) =>
        Similar[T, SchemaF, T[Diff[T, SchemaF, ?]]](Choice[DiffT[T, SchemaF]](diffTraverse(l, r))).embed.some
      case (_, _) => None
    }
  }

  private object #:: {
    def unapply[A](arg: IList[A]): Option[(A, List[A])] = arg.toList match {
      case x :: xs => Some(x -> xs)
      case _ => None
    }
  }


  val nestedChoices: Algebra[SchemaF, Fix[SchemaF]] = {
    case SchemaF.Choice(NonEmptyList(x, Fix(SchemaF.Choice(ys)) #:: Nil)) =>
      Fix[SchemaF](SchemaF.Choice(x <:: ys))
    case SchemaF.Choice(NonEmptyList(x, Fix(SchemaF.Null) #:: Nil)) =>
      Fix[SchemaF](SchemaF.Choice(NonEmptyList(x)))
    case s =>
      Fix[SchemaF](s)
  }

  val reference: AlgebraM[Writer[SchemaManifest, ?], SchemaF, Fix[SchemaF]] = {
    case SchemaF.Object(name, fields) =>
      val required = fields.toList.flatMap { case (n, schema) =>
        schema match {
          case Fix(SchemaF.Opt(_)) => None
          case _ => Some(n)
        }
      }.toSet

      Writer[SchemaManifest, Fix[SchemaF]](SchemaManifest.single(name -> SchemaManifestEntry(fields, required)), Fix[SchemaF](SchemaF.Ref(name)))
    case s =>
      Writer[SchemaManifest, Fix[SchemaF]](SchemaManifest.empty, Fix[SchemaF](s))
  }



  val rewrite: AlgebraM[Writer[SchemaManifest, ?], SchemaF, Fix[SchemaF]] =
    compose(reference, liftM(nestedChoices))

  val fromJson: CoalgebraM[Option, SchemaF, Json] = {
    case Fix(JsonF.Object(fields)) => fields match {
      case BooleanExtractor(_) => Some(SchemaF.Boolean)
      case NullExtractor(_) => Some(SchemaF.Null)
      case CollectionExtractor(items, uniqueItems, bound) =>
        Some(SchemaF.Collection(bound, uniqueItems, items))
      case IntExtractor(minimum, exclusiveMinimum, maximum, exclusiveMaximum, integerType) =>
        Some(SchemaF.Integer(Range(Bound(minimum, !exclusiveMinimum), Bound(maximum, !exclusiveMaximum)), integerType))
      case NumberExtractor(minimum, exclusiveMinimum, maximum, exclusiveMaximum, numberType) =>
        Some(SchemaF.Number(Range(Bound(minimum, !exclusiveMinimum), Bound(maximum, !exclusiveMaximum)), numberType))
      case StrFormatExtractor(t) =>
        Some(SchemaF.Str(StringDescription.Type(t)))
      case StrPatternExtractor(pattern) =>
        Some(SchemaF.Str(StringDescription.Pattern(pattern)))
      case StrBoundedLengthExtractor(bound) =>
        Some(SchemaF.Str(StringDescription.Length(bound)))
      case StrUnboundedLengthExtractor(_) =>
        Some(SchemaF.Str(StringDescription.Unbounded))
      case RefExtractor(name) =>
        Some(SchemaF.Ref(name))
      case EnumExtractor(values) =>
        Some(SchemaF.Enum(values))
      case ChoiceExtractor(choices) =>
        Some(SchemaF.Choice(choices))
    }
    case _ => None
  }

  private def boundToMapJson(lowField: String, highField: String, bound: LengthBound) = bound match {
    case LengthBound.Atleast(low) =>
      Map(lowField -> JsonF.fromInteger(low))
    case LengthBound.Atmost(high) =>
      Map(highField -> JsonF.fromInteger(high))
    case LengthBound.Interval(low, high) =>
      Map(lowField -> JsonF.fromInteger(low), highField -> JsonF.fromInteger(high))
    case LengthBound.Unbounded =>
      Map.empty[String, JsonF.Json]
  }

  val toJson: Algebra[SchemaF, Json] = {

    case SchemaF.Boolean => JsonF.obj("type" -> JsonF.fromString("boolean"))
    case SchemaF.Null => JsonF.obj("type" -> JsonF.fromString("null"))
    case SchemaF.Collection(bound, uniqueItems, schema) =>
      JsonF.fromFields(
        Map(
          "items" -> schema,
          "type" -> JsonF.fromString("array"),
          "uniqueItems" -> JsonF.fromBoolean(uniqueItems)
        ) ++ boundToMapJson("minItems", "maxItems", bound)
      )
    case SchemaF.Documented(doc, Fix(JsonF.Object(fields))) =>
      JsonF.fromFields(fields + ("description" -> JsonF.fromString(doc)))

    case SchemaF.Integer(range, t) =>
      JsonF.obj(
        "type" -> JsonF.fromString("integer"),
        "minimum" -> JsonF.fromBigDecimal(range.lower.value),
        "exclusiveMinimum" -> JsonF.fromBoolean(!range.lower.inclusive),
        "maximum" -> JsonF.fromBigDecimal(range.upper.value),
        "exclusiveMaximum" -> JsonF.fromBoolean(!range.upper.inclusive),
        "format" -> JsonF.fromString(t.format)
      )

    case SchemaF.Number(range, t) =>
      JsonF.obj(
        "type" -> JsonF.fromString("number"),
        "minimum" -> JsonF.fromBigDecimal(range.lower.value),
        "exclusiveMinimum" -> JsonF.fromBoolean(!range.lower.inclusive),
        "maximum" -> JsonF.fromBigDecimal(range.upper.value),
        "exclusiveMaximum" -> JsonF.fromBoolean(!range.upper.inclusive),
        "format" -> JsonF.fromString(t.format)
      )
    case SchemaF.Str(StringDescription.Type(t)) =>
      JsonF.obj(
        "type" -> JsonF.fromString("string"),
        "format" -> JsonF.fromString(t.format)
      )
    case SchemaF.Str(StringDescription.Pattern(pattern)) =>
      JsonF.obj(
        "type" -> JsonF.fromString("string"),
        "pattern" -> JsonF.fromString(pattern)
      )
    case SchemaF.Str(StringDescription.Length(bound)) =>
      JsonF.fromFields(boundToMapJson("minLength", "maxLength", bound) + ("type" -> JsonF.fromString("string")))

    case SchemaF.Str(StringDescription.Unbounded) =>
      JsonF.obj("type" -> JsonF.fromString("string"))

    case SchemaF.Ref(name) =>
      JsonF.obj("$ref" -> JsonF.fromString(s"#/definitions/$name"))
    case SchemaF.Object(_, fields) =>
      JsonF.obj(fields.toList: _*)
    case SchemaF.Choice(options) =>
      JsonF.obj("oneOf" -> JsonF.fromValues(options.toList))
    case SchemaF.Enum(options) =>
      JsonF.obj("type" -> JsonF.obj("enum" -> JsonF.fromValues(options.map(JsonF.fromString))))
    case SchemaF.Opt(schema) => schema
  }

  val jsonPrism: CoalgebraPrism[SchemaF, Json] = CoalgebraPrism(fromJson)(toJson)
}





