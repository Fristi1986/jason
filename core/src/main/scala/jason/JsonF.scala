package jason

import matryoshka.data.Fix

import scalaz._
import scalaz.Scalaz._

sealed trait JsonF[+A]

object JsonF {
  final case object Null extends JsonF[Nothing]
  final case class Object[A](fields: Map[String, A]) extends JsonF[A]
  final case class Str(value: String) extends JsonF[Nothing]
  final case class Array[A](values: List[A]) extends JsonF[A]
  final case class Bool(value: Boolean) extends JsonF[Nothing]
  final case class Number(number: BigDecimal) extends JsonF[Nothing]

  type Json = Fix[JsonF]
  
  def nil: Json = Fix[JsonF](Null)
  def obj(fields: (String, Json)*): Json = Fix[JsonF](Object(fields.toMap))
  def fromString(value: String): Json = Fix[JsonF](Str(value))
  def fromValues(values: Iterable[Json]): Json = Fix[JsonF](Array(values.toList))
  def fromBoolean(value: Boolean): Json = Fix[JsonF](Bool(value))
  def fromInteger(value: Int): Json = Fix[JsonF](Number(BigDecimal(value)))
  def fromBigDecimal(value: BigDecimal): Json = Fix[JsonF](Number(value))
  def fromFields(fields: Map[String, Json]): Json = Fix[JsonF](Object(fields))

  implicit val traverse: Traverse[JsonF] = new Traverse[JsonF] {
    override def traverseImpl[G[_], A, B](fa: JsonF[A])(f: A => G[B])(implicit G: Applicative[G]): G[JsonF[B]] = fa match {
      case Null => G.pure(Null)
      case Object(fields) => Traverse[Map[String, ?]].traverse(fields)(f).map(Object.apply)
      case Str(value) => G.pure(Str(value))
      case Array(values) => values.traverse(f).map(values => Array(values))
      case Bool(value) => G.pure(Bool(value))
      case Number(value) => G.pure(Number(value))
    }
  }
}
