package jason

import matryoshka.data.Fix
import shapeless.ops.coproduct.Align
import shapeless.{Coproduct, Generic}

import scala.util.Try
import scalaz._
import scalaz.Scalaz._



final case class Member[F[_], A, B] private (
                                              typeClass: F[A],
                                              getter: B => A,
                                              documentation: Option[String]
                                            ) {
  def mapTypeClass[G[_]](f: F ~> G): Member[G, A, B] = copy(typeClass = f(typeClass))
}

trait Transformer[F[_], I, O] {
  def apply(fi: F[I]): F[O]
}

object Transformer {
  implicit def alignedCoproduct[F[_], I <: Coproduct, Repr <: Coproduct, O](implicit
                                                                            I: InvariantFunctor[F],
                                                                            G: Generic.Aux[O, Repr],
                                                                            TA: Align[Repr, I],
                                                                            FA: Align[I, Repr]): Transformer[F, I, O] = new Transformer[F, I, O] {
    override def apply(fi: F[I]): F[O] =
      I.xmap[I, O](fi, a => G.from(FA(a)), b => TA(G.to(b)))
  }
}


sealed trait Attempt[+A] { self =>
  def toEither: Either[String, A] = self match {
    case Attempt.Success(value) => Right(value)
    case Attempt.Exception(ex) => Left(ex.getMessage)
    case Attempt.Error(err) => Left(err)
  }
}

object Attempt {
  final case class Success[A](value: A) extends Attempt[A]
  final case class Exception(error: Throwable) extends Attempt[Nothing] {
    override def equals(obj: scala.Any): Boolean = {
      obj match {
        case Exception(other) => other.getMessage == error.getMessage
        case _ => false
      }
    }
  }
  final case class Error(error: String) extends Attempt[Nothing]

  def exception(ex: Throwable): Attempt[Nothing] = Attempt.Exception(ex)
  def error(msg: String): Attempt[Nothing] = Attempt.Error(msg)
  def success[A](value: A): Attempt[A] = Attempt.Success(value)

  def fromTry[A](t: Try[A]): Attempt[A] = t match {
    case scala.util.Failure(err) => exception(err)
    case scala.util.Success(value) => success(value)
  }

  def fromEither[L, R](t: Either[String, R]): Attempt[R] = t match {
    case Left(err) => error(err)
    case Right(value) => success(value)
  }

  def fromOption[A](option: Option[A], ifEmpty: String): Attempt[A] = option match {
    case None => error(ifEmpty)
    case Some(value) => success(value)
  }
}


final case class SchemaManifestEntry(properties: Map[String, Fix[SchemaF]], required: Set[String])
final case class SchemaManifest(entries: Map[String, SchemaManifestEntry]) {
  def without(objectName: String) = copy(entries = entries - objectName)
}


object SchemaManifest {
  def empty = SchemaManifest(Map.empty[String, SchemaManifestEntry])

  def single(init: (String, SchemaManifestEntry)): SchemaManifest = SchemaManifest(Map(init))

  implicit def monoid: Monoid[SchemaManifest] = new Monoid[SchemaManifest] {
    override def zero: SchemaManifest = SchemaManifest.empty
    override def append(x: SchemaManifest, y: => SchemaManifest): SchemaManifest = SchemaManifest(x.entries ++ y.entries)
  }
}

sealed trait LengthBound { self =>
  def isCompatible(other: LengthBound): Boolean =
    implicitly[PartialOrdering[LengthBound]]
      .tryCompare(self, other)
      .exists(x => x == 0 || x == 1)
}

object LengthBound {
  case object Unbounded extends LengthBound
  case class Atmost(value: Int) extends LengthBound
  case class Atleast(value: Int) extends LengthBound
  case class Interval(low: Int, high: Int) extends LengthBound

  implicit val order: PartialOrdering[LengthBound] = new PartialOrdering[LengthBound] {
    override def tryCompare(x: LengthBound, y: LengthBound): Option[Int] = (x,y) match {

      case (Unbounded, Unbounded) => Some(0)

      case (Atmost(left), Atmost(right)) => Some(scala.Ordering[Int].compare(left, right))
      case (Atleast(left), Atleast(right)) => Some(scala.Ordering[Int].compare(left, right))

      case (Interval(lowLeft, highLeft), Interval(lowRight, highRight)) =>
        Some(scala.Ordering[Int].compare(lowLeft, lowRight) |+| scala.Ordering[Int].compare(highLeft, highRight))

      case (_, Unbounded) => Some(1)
      case (Unbounded, _) => Some(-1)

      case _ => None
    }

    override def lteq(x: LengthBound, y: LengthBound): Boolean = tryCompare(x, y).exists(x => x == -1 || x == 0)
  }
}

case class Bound(value: BigDecimal, inclusive: Boolean) {
  def <=(other: Bound): Boolean = value <= other.value
  def >=(other: Bound): Boolean = value >= other.value
}

case class Range(lower: Bound, upper: Bound) {
  def isCompatible(other: Range): Boolean =
    other.lower <= lower && other.upper >= upper
}

sealed abstract class IntegerType(val format: String)

object IntegerType {

  case object Int32 extends IntegerType("int32")

  case object Int64 extends IntegerType("int64")

  val all = Set(Int32, Int64)

  def fromString(value: String): Option[IntegerType] = all.find(_.format == value)

}

sealed abstract class NumberType(val format: String)

object NumberType {

  case object Float extends NumberType("float")
  case object Double extends NumberType("double")

  val all = Set(Float, Double)

  def fromString(value: String): Option[NumberType] = all.find(_.format == value)
}

sealed abstract class StringType(val format: String)

object StringType {

  case object DateTime extends StringType("date-time")

  case object Email extends StringType("email")

  case object Hostname extends StringType("hostname")

  case object IPV4 extends StringType("ipv4")

  case object IPV6 extends StringType("ipv6")

  case object Uri extends StringType("uri")

  val all = Set(DateTime, Email, Hostname, IPV4, IPV6, Uri)

  def fromString(value: String): Option[StringType] = all.find(_.format == value)
}

sealed trait StringDescription

object StringDescription {

  case class Type(stringType: StringType) extends StringDescription

  case class Length(lengthBound: LengthBound) extends StringDescription

  case class Pattern(pattern: String) extends StringDescription

  case object Unbounded extends StringDescription

}


