package jason

import java.time.Instant
import java.util.UUID

import matryoshka.data.Fix
import shapeless.CNil

import scalaz.{Contravariant, NonEmptyList}
import scalaz.Scalaz._

sealed trait JasonSchema[A] {
  def create: Fix[SchemaF]
}

object JasonSchema {

  def create[A](schema: Fix[SchemaF]): JasonSchema[A] = new JasonSchema[A] {
    def create: Fix[SchemaF] = schema
  }

  implicit val contravariant: Contravariant[JasonSchema] = new Contravariant[JasonSchema] {
    override def contramap[A, B](fa: JasonSchema[A])(f: B => A): JasonSchema[B] = new JasonSchema[B] {
      override def create: Fix[SchemaF] = fa.create
    }
  }

  implicit def interpreter = new JasonAlgebra[JasonSchema] with JasonSchemaFormatN {
    override def int(bound: Range): JasonSchema[Int] = create(Fix[SchemaF](SchemaF.Integer(bound, IntegerType.Int32)))

    override def long(bound: Range): JasonSchema[Long] = create(Fix[SchemaF](SchemaF.Integer(bound, IntegerType.Int64)))

    override def float(bound: Range): JasonSchema[Float] = create(Fix[SchemaF](SchemaF.Number(bound, NumberType.Float)))

    override def double(bound: Range): JasonSchema[Double] = create(Fix[SchemaF](SchemaF.Number(bound, NumberType.Double)))

    override def string(description: StringDescription): JasonSchema[String] = create(Fix[SchemaF](SchemaF.Str(description)))

    override val bool: JasonSchema[Boolean] = create(Fix[SchemaF](SchemaF.Boolean))
    override val cnil: JasonSchema[CNil] = create(Fix[SchemaF](SchemaF.Null))
    override val uuid: JasonSchema[UUID] = create(Fix[SchemaF](SchemaF.Str(StringDescription.Pattern("/^[A-F\\d]{8}-[A-F\\d]{4}-4[A-F\\d]{3}-[89AB][A-F\\d]{3}-[A-F\\d]{12}$/"))))
    override val instant: JasonSchema[Instant] = create(Fix[SchemaF](SchemaF.Integer(Range(Bound(0L, true), Bound(Long.MaxValue, true)), IntegerType.Int64)))

    override def option[A](from: JasonSchema[A]): JasonSchema[Option[A]] = create(Fix[SchemaF](SchemaF.Opt(from.create)))

    override def list[A](of: JasonSchema[A]): JasonSchema[List[A]] = create(Fix[SchemaF](SchemaF.Collection(LengthBound.Atleast(0), false, of.create)))

    override def set[A](of: JasonSchema[A]): JasonSchema[Set[A]] = create(Fix[SchemaF](SchemaF.Collection(LengthBound.Atleast(0), true, of.create)))

    override def vector[A](of: JasonSchema[A]): JasonSchema[Vector[A]] = create(Fix[SchemaF](SchemaF.Collection(LengthBound.Atleast(0), false, of.create)))

    override def seq[A](of: JasonSchema[A]): JasonSchema[Seq[A]] = create(Fix[SchemaF](SchemaF.Collection(LengthBound.Atleast(0), false, of.create)))

    override def pmap[A, B](fa: JasonSchema[A])(f: A => Attempt[B])(g: B => A): JasonSchema[B] = fa.contramap(g)

    override def imap[A, B](fa: JasonSchema[A])(f: A => B)(g: B => A): JasonSchema[B] = fa.contramap(g)

    override def or[A, B](fa: JasonSchema[A], fb: JasonSchema[B]): JasonSchema[Either[A, B]] = create(Fix[SchemaF](SchemaF.Choice(NonEmptyList(fa.create, fb.create))))
  }

  implicit def apply[A](implicit T: Jason[A]): JasonSchema[A] = T.apply[JasonSchema]
}