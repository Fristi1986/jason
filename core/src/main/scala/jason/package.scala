import matryoshka.{Algebra, AlgebraM, Recursive}

import scalaz._
import scalaz.Scalaz._

package object jason extends JasonDsl {

  def member[A, B](avro: Jason[A], getter: B => A, documentation: Option[String] = None): Member[Jason, A, B] =
    Member[Jason, A, B](avro, getter, documentation)



  protected [jason] def compose[M[_] : Monad, F[_] : Functor, A](alga: AlgebraM[M, F, A], algb: AlgebraM[M, F, A])(implicit T: Recursive.Aux[A, F]): AlgebraM[M, F, A] =
    s => alga(s) >>= (b => algb(T.project(b)))

  protected [jason] def liftM[M[_] : Monad, F[_], A](algebra: Algebra[F, A]): AlgebraM[M, F, A] = {
    s => Monad[M].pure(algebra(s))
  }
}
