package jason

import java.time.Instant
import java.util.UUID

import shapeless.CNil

trait JasonAlgebra[F[_]] extends JasonAlgebraFormatN[F] {
  def int(bound: Range): F[Int]
  def float(bound: Range): F[Float]
  def double(bound: Range): F[Double]
  def long(bound: Range): F[Long]

  def string(description: StringDescription): F[String]
  val bool: F[Boolean]
  val cnil: F[CNil]

  val uuid: F[UUID]
  val instant: F[Instant]

  def option[A](from: F[A]): F[Option[A]]
  def list[A](of: F[A]): F[List[A]]
  def set[A](of: F[A]): F[Set[A]]
  def vector[A](of: F[A]): F[Vector[A]]
  def seq[A](of: F[A]): F[Seq[A]]

  def pmap[A,B](fa: F[A])(f: A => Attempt[B])(g: B => A): F[B]
  def imap[A,B](fa: F[A])(f: A => B)(g: B => A): F[B]
  def or[A, B](fa: F[A], fb: F[B]): F[Either[A, B]]
}

object JasonAlgebra {
  def apply[F[_]](implicit F: JasonAlgebra[F]): JasonAlgebra[F] = F
}