package jason

import scalaz._
import scalaz.Scalaz._
import matryoshka.data.Fix
import matryoshka.{Algebra, patterns}
import matryoshka.implicits._
import matryoshka.patterns._
import SchemaCompatibilityF._


sealed trait SchemaCompatibilityF[+A]

object SchemaCompatibilityF {
  case object Compatible extends SchemaCompatibilityF[Nothing]
  final case class RangeIncompatible(oldRange: Range, newRange: Range) extends SchemaCompatibilityF[Nothing]
  final case class LengthBoundIncompatible(oldLengthBound: LengthBound, newLengthBound: LengthBound) extends SchemaCompatibilityF[Nothing]
  final case class ObjectNameDifferent(oldName: String, newName: String) extends SchemaCompatibilityF[Nothing]
  final case class CoproductIncompatible(oldEntries: Set[String], newEntries: Set[String]) extends SchemaCompatibilityF[Nothing]
  final case class NewFieldsNotOptional(fields: Set[String]) extends SchemaCompatibilityF[Nothing]
  final case class RefIncompatible(oldRef: String, newRef: String) extends SchemaCompatibilityF[Nothing]
  final case class StringTypeIncompatible(oldFormat: StringType, newFormat: StringType) extends SchemaCompatibilityF[Nothing]
  final case class PotentialPatternDiscrepancy(oldPattern: String, newPattern: String) extends SchemaCompatibilityF[Nothing]
  final case class StringWidening(newDescription: StringDescription) extends SchemaCompatibilityF[Nothing]
  final case object CannotGuaranteeUniquenessInCollection extends SchemaCompatibilityF[Nothing]
  final case class ObjectCompatibility[A](map: Map[String, A]) extends SchemaCompatibilityF[A]
  final case class ProductCompatibility[A](root: A, manifest: Map[String, A]) extends SchemaCompatibilityF[A]
  final case class CoproductCompatibility[A](manifest: Map[String, A]) extends SchemaCompatibilityF[A]

  type SchemaCompatibility = Fix[SchemaCompatibilityF]

  def compatible: SchemaCompatibility =
    Fix[SchemaCompatibilityF](SchemaCompatibilityF.Compatible)
  def cannotGuaranteeUniquenessInCollection: SchemaCompatibility =
    Fix[SchemaCompatibilityF](SchemaCompatibilityF.CannotGuaranteeUniquenessInCollection)
  def rangeIncompatible(oldRange: Range, newRange: Range): SchemaCompatibility =
    Fix[SchemaCompatibilityF](SchemaCompatibilityF.RangeIncompatible(oldRange, newRange))
  def lengthBoundIncompatible(oldLengthBound: LengthBound, newLengthBound: LengthBound): SchemaCompatibility =
    Fix[SchemaCompatibilityF](SchemaCompatibilityF.LengthBoundIncompatible(oldLengthBound, newLengthBound))
  def objectNameDifferent(oldName: String, newName: String): SchemaCompatibility =
    Fix[SchemaCompatibilityF](SchemaCompatibilityF.ObjectNameDifferent(oldName, newName))
  def refIncompatible(oldRef: String, newRef: String): SchemaCompatibility =
    Fix[SchemaCompatibilityF](SchemaCompatibilityF.RefIncompatible(oldRef, newRef))
  def stringTypeIncompatible(oldType: StringType, newType: StringType): SchemaCompatibility =
    Fix[SchemaCompatibilityF](SchemaCompatibilityF.StringTypeIncompatible(oldType, newType))
  def stringWidening(newDescripton: StringDescription): SchemaCompatibility =
    Fix[SchemaCompatibilityF](SchemaCompatibilityF.StringWidening(newDescripton))
  def potentialPatternDiscrepancy(oldPattern: String, newPattern: String): SchemaCompatibility =
    Fix[SchemaCompatibilityF](SchemaCompatibilityF.PotentialPatternDiscrepancy(oldPattern, newPattern))
  def objectCompatibility(map: Map[String, SchemaCompatibility]): SchemaCompatibility =
    Fix[SchemaCompatibilityF](SchemaCompatibilityF.ObjectCompatibility(map))
  def coproductIncompatible(oldEntries: Set[String], newEntries: Set[String]): SchemaCompatibility =
    Fix[SchemaCompatibilityF](SchemaCompatibilityF.CoproductIncompatible(oldEntries, newEntries))
  def newFieldsNotOptional(newFields: Set[String]): SchemaCompatibility =
    Fix[SchemaCompatibilityF](SchemaCompatibilityF.NewFieldsNotOptional(newFields))
  def productCompatibility(root: SchemaCompatibility, manifest: Map[String, SchemaCompatibility]): SchemaCompatibility =
    Fix[SchemaCompatibilityF](SchemaCompatibilityF.ProductCompatibility(root, manifest))
  def coproductCompatibility(manifest: Map[String, SchemaCompatibility]): SchemaCompatibility =
    Fix[SchemaCompatibilityF](SchemaCompatibilityF.CoproductCompatibility(manifest))
}

trait SchemaCompatibilityChecker[A] {
  def check(a: A, b: A): Fix[SchemaCompatibilityF]
}

object SchemaCompatibilityChecker {

  private def rangeCompatibility(a: Range, b: Range): SchemaCompatibility =
    if(a.isCompatible(b)) compatible
    else rangeIncompatible(a, b)

  private def lengthBoundCompatiblity(a: LengthBound, b: LengthBound): SchemaCompatibility =
    if(a.isCompatible(b)) compatible
    else lengthBoundIncompatible(a, b)

  type SchemaDiff[A] = Diff[Fix, SchemaF, A]

  private object NumericCompat {
    def unapply[A](diff: SchemaDiff[A]): Option[SchemaCompatibility] = {
      diff match {
        case LocallyDifferent(SchemaF.Integer(a, _), SchemaF.Integer(b, _)) =>
          Some(rangeCompatibility(a, b))
        case LocallyDifferent(SchemaF.Number(a, _), SchemaF.Number(b, _)) =>
          Some(rangeCompatibility(a, b))
        case Different(Fix(SchemaF.Integer(a, _)), Fix(SchemaF.Number(b, _))) =>
          Some(rangeCompatibility(a, b))
        case Different(Fix(SchemaF.Number(a, _)), Fix(SchemaF.Integer(b, _))) =>
          Some(rangeCompatibility(a, b))
        case _ => None
      }
    }
  }

  private object StringCompat {
    def unapply[A](diff: SchemaDiff[A]): Option[SchemaCompatibility] = {
      diff match {
        case LocallyDifferent(SchemaF.Str(StringDescription.Pattern(left)), SchemaF.Str(StringDescription.Pattern(right))) if left != right =>
          Some(potentialPatternDiscrepancy(left, right))
        case LocallyDifferent(SchemaF.Str(StringDescription.Type(left)), SchemaF.Str(StringDescription.Type(right))) if left != right =>
          Some(stringTypeIncompatible(left, right))
        case LocallyDifferent(SchemaF.Str(StringDescription.Length(left)), SchemaF.Str(StringDescription.Length(right))) =>
          Some(lengthBoundCompatiblity(left, right))
        case LocallyDifferent(SchemaF.Str(StringDescription.Unbounded), SchemaF.Str(newDesc)) =>
          Some(stringWidening(newDesc))
        case LocallyDifferent(SchemaF.Str(_), SchemaF.Str(StringDescription.Unbounded)) =>
          Some(compatible)
        case _ => None
      }
    }
  }

  private object RefCompat {
    def unapply[A](diff: SchemaDiff[A]): Option[(String, String)] = {
      diff match {
        case Different(Fix(SchemaF.Ref(left)), Fix(SchemaF.Opt(Fix(SchemaF.Ref(right))))) =>
          Some(left -> right)
        case _ => None
      }
    }
  }

  private object CollectionCompat {
    def unapply[A](diff: SchemaDiff[SchemaCompatibility]): Option[SchemaCompatibility] = {
      diff match {
        case Similar(SchemaF.Collection(_, _, compat)) =>
          Some(compat)
        case LocallyDifferent(SchemaF.Collection(_, true, _), SchemaF.Collection(_, false, _)) =>
          Some(compatible)
        case LocallyDifferent(SchemaF.Collection(_, false, _), SchemaF.Collection(_, true, _)) =>
          Some(cannotGuaranteeUniquenessInCollection)
        case LocallyDifferent(SchemaF.Collection(oldBound, _, _), SchemaF.Collection(newBound, _, _)) =>
          Some(lengthBoundCompatiblity(oldBound, newBound))
        case _ => None
      }
    }
  }

  private def diff(a: Fix[SchemaF], b: Fix[SchemaF]): Fix[Diff[Fix, SchemaF, ?]] = a.paraMerga(b)(patterns.diff)

  def algebra(oldManifest: SchemaManifest, newManifest: SchemaManifest): Algebra[Diff[Fix, SchemaF, ?], SchemaCompatibility] = {
    case Same(_) => compatible
    case NumericCompat(compatibility) => compatibility
    case StringCompat(compatibility) => compatibility
    case CollectionCompat(compatibility) => compatibility
    case RefCompat(leftId, rightId) =>
      (oldManifest.entries.get(leftId) |@| newManifest.entries.get(rightId))(checkManifestEntry(oldManifest, newManifest)).getOrElse(refIncompatible(leftId, rightId))
  }

  private def checkManifestEntry(oldManifest: SchemaManifest, newManifest: SchemaManifest)
                                (oldEntry: SchemaManifestEntry, newEntry: SchemaManifestEntry): Fix[SchemaCompatibilityF] = {



    if(oldEntry.required == newEntry.required) {
      objectCompatibility(foldZippedMaps(oldEntry.properties, newEntry.properties)(diff(_,_).cata(algebra(oldManifest, newManifest))))
    } else {
      val newProps = newEntry.required -- oldEntry.required
      val inCompatibleFields = newProps.filter {
        key => newEntry.properties(key) match {
          case Fix(SchemaF.Opt(_)) => false
          case _ => true
        }
      }
      if(inCompatibleFields.nonEmpty) {
        newFieldsNotOptional(inCompatibleFields)
      } else {
        objectCompatibility(foldZippedMaps(oldEntry.properties, newEntry.properties)(diff(_,_).cata(algebra(oldManifest, newManifest))))
      }
    }
  }

  implicit val schemaF: SchemaCompatibilityChecker[Fix[SchemaF]] = new SchemaCompatibilityChecker[Fix[SchemaF]] {

    override def check(a: Fix[SchemaF], b: Fix[SchemaF]): Fix[SchemaCompatibilityF] = {
      val (manifestA, rootA): (SchemaManifest, Fix[SchemaF]) = a.cataM(SchemaF.rewrite).run
      val (manifestB, rootB): (SchemaManifest, Fix[SchemaF]) = b.cataM(SchemaF.rewrite).run
      val checker = checkManifestEntry(manifestA, manifestB) _

      (rootA, rootB) match {
        case (Fix(SchemaF.Ref(left)), Fix(SchemaF.Ref(right))) =>
          productCompatibility(
            checker(manifestA.entries(left), manifestB.entries(right)),
            foldZippedMaps(manifestA.entries, manifestB.entries)(checker(_,_))
          )

        case (Fix(SchemaF.Choice(left)), Fix(SchemaF.Choice(right))) =>
          val oldRefs = left.list.toList.collect { case Fix(SchemaF.Ref(name)) => name }.toSet
          val newRefs = right.list.toList.collect { case Fix(SchemaF.Ref(name)) => name }.toSet
          if(oldRefs == newRefs) coproductCompatibility(foldZippedMaps(manifestA.entries, manifestB.entries)(checker(_,_)))
          else coproductIncompatible(oldRefs, newRefs)
        case _ => ???
      }

    }
  }

  implicit val jsonSchema: SchemaCompatibilityChecker[JsonSchema] = new SchemaCompatibilityChecker[JsonSchema] {


    override def check(a: JsonSchema, b: JsonSchema): Fix[SchemaCompatibilityF] = (a, b) match {
      case (JsonSchema.Product(rootA, manifestA), JsonSchema.Product(rootB, manifestB)) =>
        val checker = checkManifestEntry(SchemaManifest(manifestA), SchemaManifest(manifestB)) _

        productCompatibility(
          checker(rootA, rootB),
          foldZippedMaps(manifestA, manifestB)(checker(_,_))
        )

      case (JsonSchema.Coproduct(left, manifestA), JsonSchema.Coproduct(right, manifestB)) =>
        val oldRefs = left.list.toList.collect { case Fix(SchemaF.Ref(name)) => name }.toSet
        val newRefs = right.list.toList.collect { case Fix(SchemaF.Ref(name)) => name }.toSet
        val checker = checkManifestEntry(SchemaManifest(manifestA), SchemaManifest(manifestB)) _

        if(oldRefs == newRefs) coproductCompatibility(foldZippedMaps(manifestA, manifestB)(checker(_,_)))
        else coproductIncompatible(oldRefs, newRefs)

      case _ => ???
    }
  }

  private def foldZippedMaps[A, B](a: Map[String, A], b: Map[String, A])(f: (A, A) => B): Map[String, B] =
    (a.keySet intersect b.keySet).map { key => key -> f(a(key), b(key)) }.toMap

  def apply[T](implicit T: SchemaCompatibilityChecker[T]): SchemaCompatibilityChecker[T] = T
}