package jason

import jason.JsonF.Json
import matryoshka.data.Fix
import matryoshka.implicits._
import monocle.Prism

import scalaz._
import scalaz.Scalaz._

sealed trait JsonSchema

object JsonSchema {

  final case class Product(single: SchemaManifestEntry, definitions: Map[String, SchemaManifestEntry]) extends JsonSchema
  final case class Coproduct(choices: NonEmptyList[Fix[SchemaF]], definitions: Map[String, SchemaManifestEntry]) extends JsonSchema

  private def getObjectDescriptor(json: JsonF.Json): Option[SchemaManifestEntry] = json match {
    case Fix(JsonF.Object(fields)) =>
      for {
        required <- fields.get("required").collect { case Fix(JsonF.Array(v)) => v } map { _.collect { case Fix(JsonF.Str(value)) => value }.toSet }
        props <- fields
          .get("properties")
          .collect { case Fix(JsonF.Object(v)) => v }
          .flatMap { x => Traverse[Map[String, ?]].traverse(x)(_.anaM[Fix[SchemaF]](SchemaF.fromJson))}
      } yield SchemaManifestEntry(props, required)
    case _ => None
  }

  private def getDefinitions(json: JsonF.Json): Option[Map[String, SchemaManifestEntry]] = json match {
    case Fix(JsonF.Object(fields)) =>
      fields
        .get("definitions")
        .collect { case Fix(JsonF.Object(v)) => v }
        .flatMap(map => Traverse[Map[String, ?]].traverse(map)(getObjectDescriptor))
    case _ => None
  }

  private def listToNonEmptyList[A](list: List[A]): Option[NonEmptyList[A]] = list match {
    case x :: xs => Some(NonEmptyList(x, xs : _*))
    case _ => None
  }

  private def getChoices(json: JsonF.Json): Option[NonEmptyList[Fix[SchemaF]]] = json match {
    case Fix(JsonF.Object(fields)) =>
      fields
        .get("oneOf")
        .collect { case Fix(JsonF.Array(v)) => v }
        .flatMap(_.traverse[Option, Fix[SchemaF]](_.anaM[Fix[SchemaF]](SchemaF.fromJson)).flatMap(listToNonEmptyList))
    case _ => None
  }

  private object ProductExtractor {
    def unapply(json: JsonF.Json): Option[Product] =
      (getObjectDescriptor(json) |@| getDefinitions(json))(Product.apply)
  }

  private object CoproductExtractor {
    def unapply(json: JsonF.Json): Option[Coproduct] =
      (getChoices(json) |@| getDefinitions(json))(Coproduct.apply)
  }

  def fromJson(json: JsonF.Json): Option[JsonSchema] = json match {
    case ProductExtractor(p) => Some(p)
    case CoproductExtractor(c) => Some(c)
    case _ => None
  }

  def toJson(schema: JsonSchema): JsonF.Json = {
    def definitionsToJson(map: Map[String, SchemaManifestEntry]): JsonF.Json =
      JsonF.fromFields(
        map.mapValues(desc =>
          JsonF.obj(
            "required" -> JsonF.fromValues(desc.required.map(JsonF.fromString)),
            "properties" -> JsonF.fromFields(desc.properties.mapValues(_.cata(SchemaF.toJson)))
          )
        )
      )

    schema match {
      case Product(desc, defs) =>
        JsonF.fromFields(
          Map(
            "properties" -> JsonF.fromFields(desc.properties.mapValues(_.cata(SchemaF.toJson))),
            "required" -> JsonF.fromValues(desc.required.map(JsonF.fromString)),
            "definitions" -> definitionsToJson(defs)
          )
        )

      case Coproduct(choices, defs) =>
        JsonF.fromFields(
          Map(
            "oneOf" -> JsonF.fromValues(choices.toList.map(_.cata(SchemaF.toJson))),
            "definitions" -> definitionsToJson(defs)
          )
        )
    }
  }

  val prism: Prism[Json, JsonSchema] = Prism(fromJson)(toJson)

  def fromSchema(schema: Fix[SchemaF]): Option[JsonSchema] = {
    def unwrapRoot(root: Fix[SchemaF], manifest: SchemaManifest): Option[(SchemaManifest, Either[NonEmptyList[Fix[SchemaF]], SchemaManifestEntry])] = root match {
      case Fix(SchemaF.Ref(name)) =>
        manifest.entries.get(name).map(entry => manifest.without(name) -> Right(entry))
      case Fix(SchemaF.Choice(choices)) =>
        Some(manifest -> Left(choices))
      case _ => None
    }

    val cleanedSchema: Writer[SchemaManifest, Fix[SchemaF]] = schema.cataM(SchemaF.rewrite)
    val (manifest, root) = cleanedSchema.run

    unwrapRoot(root, manifest) match {
      case Some((m, Left(choices))) =>
        Some(JsonSchema.Coproduct(choices, m.entries))
      case Some((m, Right(single))) =>
        Some(JsonSchema.Product(single, m.entries))
      case None => None
    }
  }

  def fromJasonSchema[A](implicit T: JasonSchema[A]): Option[JsonSchema] = fromSchema(T.create)
}