package jason

import java.time.Instant
import java.util.UUID

import io.circe.{Decoder, Encoder, Json}
import matryoshka._
import matryoshka.implicits._
import monocle.Prism
import shapeless.CNil

import scala.util.Try

package object circe {
  private implicit val encoderInterpreter: JasonAlgebra[Encoder] = new JasonAlgebra[Encoder] with CirceEncoderObjectN {
    override def int(bound: Range): Encoder[Int] = Encoder.encodeInt

    override def float(bound: Range): Encoder[Float] = Encoder.encodeFloat

    override def double(bound: Range): Encoder[Double] = Encoder.encodeDouble

    override def long(bound: Range): Encoder[Long] = Encoder.encodeLong

    override def string(description: StringDescription): Encoder[String] = Encoder.encodeString

    override val bool: Encoder[Boolean] = Encoder.encodeBoolean

    override val cnil: Encoder[CNil] = new Encoder[CNil] {
      override def apply(a: CNil): Json = Json.Null
    }

    override val uuid: Encoder[UUID] = Encoder.encodeUUID
    override val instant: Encoder[Instant] = Encoder.encodeLong.contramap(_.toEpochMilli)

    override def option[A](from: Encoder[A]): Encoder[Option[A]] = Encoder.encodeOption(from)

    override def list[A](of: Encoder[A]): Encoder[List[A]] = Encoder.encodeList(of)

    override def set[A](of: Encoder[A]): Encoder[Set[A]] = Encoder.encodeSet(of)

    override def vector[A](of: Encoder[A]): Encoder[Vector[A]] = Encoder.encodeVector(of)

    override def seq[A](of: Encoder[A]): Encoder[Seq[A]] = Encoder.encodeSeq(of)

    override def pmap[A, B](fa: Encoder[A])(f: A => Attempt[B])(g: B => A): Encoder[B] = fa.contramap(g)

    override def imap[A, B](fa: Encoder[A])(f: A => B)(g: B => A): Encoder[B] = fa.contramap(g)

    override def or[A, B](fa: Encoder[A], fb: Encoder[B]): Encoder[Either[A, B]] = new Encoder[Either[A, B]] {
      override def apply(a: Either[A, B]): Json = a match {
        case Left(left) => fa(left)
        case Right(right) => fb(right)
      }
    }
  }


  private implicit val decoderInterpreter: JasonAlgebra[Decoder] = new JasonAlgebra[Decoder] with CirceDecoderObjectN {
    override def int(bound: Range): Decoder[Int] = Decoder.decodeInt

    override def float(bound: Range): Decoder[Float] = Decoder.decodeFloat

    override def double(bound: Range): Decoder[Double] = Decoder.decodeDouble

    override def long(bound: Range): Decoder[Long] = Decoder.decodeLong

    override def string(description: StringDescription): Decoder[String] = Decoder.decodeString

    override val bool: Decoder[Boolean] = Decoder.decodeBoolean

    override val cnil: Decoder[CNil] = Decoder.const(null)

    override val uuid: Decoder[UUID] = Decoder.decodeUUID
    override val instant: Decoder[Instant] = Decoder.decodeLong.emapTry(e => Try(Instant.ofEpochMilli(e)))

    override def option[A](from: Decoder[A]): Decoder[Option[A]] = Decoder.decodeOption(from)

    override def list[A](of: Decoder[A]): Decoder[List[A]] = Decoder.decodeList(of)

    override def set[A](of: Decoder[A]): Decoder[Set[A]] = Decoder.decodeSet(of)

    override def vector[A](of: Decoder[A]): Decoder[Vector[A]] = Decoder.decodeVector(of)

    override def seq[A](of: Decoder[A]): Decoder[Seq[A]] = Decoder.decodeSeq(of)

    override def pmap[A, B](fa: Decoder[A])(f: A => Attempt[B])(g: B => A): Decoder[B] = fa.emap(a => f(a).toEither)

    override def imap[A, B](fa: Decoder[A])(f: A => B)(g: B => A): Decoder[B] = fa.map(f)

    override def or[A, B](fa: Decoder[A], fb: Decoder[B]): Decoder[Either[A, B]] = fa.map(Left.apply) or fb.map(Right.apply)
  }

  implicit def jasonToEncoder[A](implicit T : Jason[A]): Encoder[A] = T.apply[Encoder]

  implicit def jasonToDecoder[A](implicit T : Jason[A]): Decoder[A] = T.apply[Decoder]

  val toJson: Algebra[JsonF, Json] = {
    case JsonF.Object(fields) => Json.fromFields(fields.toList)
    case JsonF.Null => Json.Null
    case JsonF.Str(value) => Json.fromString(value)
    case JsonF.Bool(value) => Json.fromBoolean(value)
    case JsonF.Number(value) => Json.fromBigDecimal(value.bigDecimal)
    case JsonF.Array(values) => Json.fromValues(values)
  }

  val fromJson: CoalgebraM[Option, JsonF, Json] = {
    _.fold(
      Some(JsonF.Null),
      x => Some(JsonF.Bool(x)),
      x => x.toBigDecimal.map(JsonF.Number.apply),
      x => Some(JsonF.Str(x)),
      x => Some(JsonF.Array(x.toList)),
      x => Some(JsonF.Object(x.toMap))
    )
  }

  val jsonPrism: Prism[Json, JsonF.Json] =
    unfoldPrism[JsonF.Json, JsonF, Json](CoalgebraPrism(fromJson)(toJson))
}
