package jason

import eu.timepit.refined.api.{RefType, Refined, Validate}
import eu.timepit.refined.boolean.Not
import eu.timepit.refined.numeric.{Greater, Interval, Less, NonPositive, Positive}
import eu.timepit.refined.string.{MatchesRegex, Url}
import shapeless.Witness

package object refined {
  implicit class RichStringJason(val jason: Jason[String]) {

    private def withRefined[P](description: StringDescription)(implicit V: Validate[String, P], R: RefType[Refined]): Jason[String Refined P] =
      new Jason[Refined[String, P]] {
        override def apply[F[_] : JasonAlgebra]: F[Refined[String, P]] =
          JasonAlgebra[F].pmap(JasonAlgebra[F].string(description))(p => Attempt.fromEither(R.refine(p)))(R.unwrap)
      }

    def matchesRegex[S <: String](implicit V: Validate.Plain[String, MatchesRegex[S]], R: RefType[Refined], WS: Witness.Aux[S]): Jason[String Refined MatchesRegex[S]] =
      withRefined(StringDescription.Pattern(WS.value))

    def url[S <: String](implicit V: Validate.Plain[String, Url], R: RefType[Refined]): Jason[String Refined Url] =
      withRefined(StringDescription.Type(StringType.Uri))
  }

  implicit class RichIntJason(val jason: Jason[Int]) {
    private def withRefined[P](bound: Range)(implicit V: Validate[Int, P], R: RefType[Refined]): Jason[Int Refined P] =
      new Jason[Refined[Int, P]] {
        override def apply[F[_] : JasonAlgebra]: F[Refined[Int, P]] =
          JasonAlgebra[F].pmap(JasonAlgebra[F].int(bound))(p => Attempt.fromEither(R.refine(p)))(R.unwrap)
      }

    def positive[N <: Int](implicit V: Validate.Plain[Int, Positive]): Jason[Int Refined Positive] =
      withRefined(Range(Bound(1, true), Bound(Int.MaxValue, true)))

    def intervalOpen[L <: Int, H <: Int](implicit VL: Validate.Plain[Int, Greater[L]], VH: Validate.Plain[Int, Less[H]], L: Witness.Aux[L], H: Witness.Aux[H]): Jason[Int Refined Interval.Open[L, H]] =
      withRefined(Range(Bound(L.value, true), Bound(H.value, true)))
  }
}
