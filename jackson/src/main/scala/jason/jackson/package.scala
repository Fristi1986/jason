package jason

import matryoshka.{Algebra, CoalgebraM, CoalgebraPrism, unfoldPrism}
import monocle.Prism
import org.codehaus.jackson.{JsonFactory, JsonNode}
import org.codehaus.jackson.node._
import scala.collection.JavaConverters._

import scala.util.control.NonFatal

package object jackson {
  val fromJson: CoalgebraM[Option, JsonF, JsonNode] = {
    case u : DecimalNode => Some(JsonF.Number(BigDecimal(u.getDecimalValue)))
    case u : ObjectNode => Some(JsonF.Object(u.getFields.asScala.map(e => e.getKey -> e.getValue).toMap))
    case u : BooleanNode => Some(JsonF.Bool(u.getBooleanValue))
    case _ : NullNode => Some(JsonF.Null)
    case u : ArrayNode => Some(JsonF.Array(u.getElements.asScala.toList))
    case u : TextNode => Some(JsonF.Str(u.getTextValue))
    case _ => None
  }

  val toJson: Algebra[JsonF, JsonNode] = {
    case JsonF.Number(value) => DecimalNode.valueOf(value.bigDecimal)
    case JsonF.Object(fields) => new ObjectNode(JsonNodeFactory.instance).putAll(fields.asJava)
    case JsonF.Str(value) => new TextNode(value)
    case JsonF.Array(values) => new ArrayNode(JsonNodeFactory.instance).addAll(values.asJava)
    case JsonF.Bool(value) => BooleanNode.valueOf(value)
    case JsonF.Null => NullNode.instance
  }

  val jsonPrism: Prism[JsonNode, JsonF.Json] =
    unfoldPrism[JsonF.Json, JsonF, JsonNode](CoalgebraPrism(fromJson)(toJson))
}
