jason
---


[![Build Status](https://api.travis-ci.org/vectos/jason.svg)](https://travis-ci.org/vectos/jason)
[![codecov.io](http://codecov.io/github/vectos/jason/coverage.svg?branch=master)](http://codecov.io/github/vectos/jason?branch=master)

_jason_ is a EDSL (embedded domain specific language) for describing Jason data types.

