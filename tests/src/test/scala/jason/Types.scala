package jason

import java.time.{Instant, LocalDate, LocalDateTime, ZonedDateTime}
import java.util.UUID

import matryoshka.data.Fix
import org.scalacheck.{Arbitrary, Gen}

import scalaz.NonEmptyList

case class AllTypes(
                     bool: Boolean,
                     string: String,
                     int: Int,
                     long: Long,
                     float: Float,
                     double: Double,
                     uuid: UUID,
                     instant: Instant,
                     localDate: LocalDate,
                     localDateTime: LocalDateTime,
                     zonedDateTime: ZonedDateTime,
                     list: List[Long],
                     set: Set[Long],
                     vector: Vector[Long],
                     option: Option[Long],
                     seq: Seq[Long],
                     either: Either[String, Long]
                   )

sealed trait User

object User {
  case class Admin(name: String) extends User
  case class Normal(name: String) extends User
  case class Super(name: String) extends User
}

trait DomainFormats {
  implicit val formatAllTypes: Jason[AllTypes] = object17("AllTypes")(AllTypes.apply)(
    "bool" -> member(bool, _.bool),
    "string" -> member(string, _.string),
    "int" -> member(int, _.int),
    "long" -> member(long, _.long),
    "float" -> member(float, _.float),
    "double" -> member(double, _.double),
    "uuid" -> member(uuid, _.uuid),
    "instant" -> member(instant, _.instant),
    "localDate" -> member(localDate, _.localDate),
    "localDateTime" -> member(localDateTime, _.localDateTime),
    "zonedDateTime" -> member(zonedDateTime, _.zonedDateTime),
    "list" -> member(list(long), _.list),
    "set" -> member(set(long), _.set),
    "vector" -> member(vector(long), _.vector),
    "option" -> member(option(long), _.option),
    "seq" -> member(seq(long), _.seq),
    "either" -> member(or(string, long), _.either)
  )

  def genInstant = Gen.chooseNum(0, Int.MaxValue).map(ts => Instant.ofEpochMilli(ts.toLong))
  def genLong = Gen.chooseNum(Long.MinValue, Long.MaxValue)

  implicit val arbAllTypes: Arbitrary[AllTypes] = Arbitrary {
    for {
      bool <- Gen.oneOf(0, 1).map(_ % 1 == 0)
      string <- Gen.alphaStr
      int <- Gen.chooseNum(Int.MinValue, Int.MaxValue)
      long <- genLong
      float <- Gen.chooseNum(Float.MinValue, Float.MaxValue)
      double <- Gen.chooseNum(Double.MinValue, Double.MaxValue)
      uuid <- Gen.uuid
      instant <- genInstant
      localDate <- Gen.const(LocalDate.now())
      localDateTime <- Gen.const(LocalDateTime.now())
      zonedDateTime <- Gen.const(ZonedDateTime.now())
      list <- Gen.listOf(genLong)
      set <- Gen.listOf(genLong).map(_.toSet)
      vector <- Gen.listOf(genLong).map(_.toVector)
      option <- Gen.option(genLong)
      seq <- Gen.listOf(genLong).map(_.toSeq)
      either <- Gen.oneOf(0, 1).flatMap {
        case 0 => Gen.alphaStr.map(Left.apply)
        case 1 => genLong.map(Right.apply)
      }
    } yield AllTypes(bool, string, int, long, float, double, uuid, instant, localDate, localDateTime, zonedDateTime, list, set, vector, option, seq, either)
  }

  private val admin = object1("Admin")(User.Admin.apply)("adminName" -> member(string, _.name))
  private val normal = object1("Normal")(User.Normal.apply)("normalName" -> member(string, _.name))
  private val superr = object1("Super")(User.Super.apply)("superName" -> member(string, _.name))

  implicit val user: Jason[User] = (admin | normal | superr).as[User]
}

object Schema {
  import SchemaF._

  type Schema = Fix[SchemaF]

  def choice(choices: NonEmptyList[Schema]): Schema = Fix(Choice(choices))
  def opt(schema: Schema): Schema = Fix(Opt(schema))
  def obj(name: String, fields: Map[String, Schema]): Schema = Fix(Object(name, fields))
  def collection(bound: LengthBound, uniqueItems: Boolean, schema: Schema): Schema = Fix(Collection(bound, uniqueItems, schema))
  def documented(doc: String, schema: Schema): Schema = Fix(Documented(doc, schema))
  def ref(id: String): Schema = Fix[SchemaF](Ref(id))
  def enum(choices: Set[String]): Schema = Fix[SchemaF](Enum(choices))
  def str(description: StringDescription): Schema = Fix[SchemaF](Str(description))
  def number(range: Range, numberType: NumberType): Schema = Fix[SchemaF](Number(range, numberType))
  def integer(range: Range, integerType: IntegerType): Schema = Fix[SchemaF](Integer(range, integerType))
  def bool: Schema = Fix[SchemaF](Boolean)
  def nil: Schema = Fix[SchemaF](Null)
}
