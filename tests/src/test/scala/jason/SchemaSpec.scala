package jason

import org.scalatest.{Matchers, WordSpec}
import matryoshka._
import matryoshka.data.Fix
import matryoshka.implicits._
import org.scalacheck.{Arbitrary, Gen}
import org.scalatest.prop.GeneratorDrivenPropertyChecks

import scalaz.NonEmptyList

class SchemaSpec extends WordSpec with Matchers with GeneratorDrivenPropertyChecks with JasonArbitraryHelpers {
  "Schema" should {

    "rewrite" in {
      val (manifest, root) = choices.cataM(SchemaF.rewrite).run

      manifest shouldBe SchemaManifest(
        Map(
          "Super" -> SchemaManifestEntry(
            properties = Map("superName" -> Schema.str(StringDescription.Unbounded)),
            required = Set("superName")
          ),
          "Admin" -> SchemaManifestEntry(
            properties = Map("adminName" -> Schema.str(StringDescription.Unbounded)),
            required = Set("adminName")
          ),
          "Normal" -> SchemaManifestEntry(
            properties = Map("normalName" -> Schema.str(StringDescription.Unbounded)),
            required = Set("normalName")
          )
        )
      )
      root shouldBe Schema.choice(NonEmptyList(Schema.ref("Super"), Schema.ref("Admin"), Schema.ref("Normal")))
    }

    "reference" in {
      val addressProps = Map(
        "street" -> Schema.str(StringDescription.Unbounded),
        "houseNumber" -> Schema.integer(Range(Bound(0, true), Bound(2000, true)), IntegerType.Int32)
      )
      val person = Schema.obj("Person",
        Map(
          "name" -> Schema.str(StringDescription.Unbounded),
          "address" -> Schema.obj("Address", addressProps)
        )
      )
      val (manifest, root) = person.cataM(SchemaF.reference).run

      manifest shouldBe SchemaManifest(
        Map(
          "Address" -> SchemaManifestEntry(
            properties = addressProps,
            required = Set("street", "houseNumber")
          ),
          "Person" -> SchemaManifestEntry(
            properties = Map(
              "name" -> Schema.str(StringDescription.Unbounded),
              "address" -> Schema.ref("Address")
            ),
            required = Set("name", "address")
          )
        )
      )
      root shouldBe Schema.ref("Person")
    }

    "nestedChoices" in {
      choices.cata(SchemaF.nestedChoices) shouldBe Schema.choice(
        NonEmptyList(
          Schema.obj("Super", Map("superName" -> Schema.str(StringDescription.Unbounded))),
          Schema.obj("Admin", Map("adminName" -> Schema.str(StringDescription.Unbounded))),
          Schema.obj("Normal", Map("normalName" -> Schema.str(StringDescription.Unbounded)))
        )
      )
    }

    "jsonPrism should not lose data" in {
      forAll { (a: SchemaF[JsonF.Json]) =>
        SchemaF.jsonPrism.getOption(SchemaF.jsonPrism.reverseGet(a)).contains(a)
      }
    }

    implicit def schemaF: Arbitrary[SchemaF[JsonF.Json]] = {

      def nel[A](gen: Gen[A]): Gen[NonEmptyList[A]] = Gen.nonEmptyListOf(gen).map(xs => NonEmptyList(xs.head, xs.tail : _*))

      def choices: Gen[SchemaF[JsonF.Json]] =
        nel(Gen.alphaStr.map(str => JsonF.obj("$ref" -> JsonF.fromString(s"#/definitions/$str")))).map(SchemaF.Choice.apply)

      def enum: Gen[SchemaF[JsonF.Json]] =
        Gen.nonEmptyListOf(Gen.alphaStr).map(xs => SchemaF.Enum(xs.toSet))

      def number: Gen[SchemaF[JsonF.Json]] = for {
        nt <- Gen.oneOf(0, 1).map {
          case 0 => NumberType.Double
          case 1 => NumberType.Float
        }
        r <- range.arbitrary
      } yield SchemaF.Number(r, nt)

      def integer: Gen[SchemaF[JsonF.Json]] = for {
        nt <- Gen.oneOf(0, 1).map {
          case 0 => IntegerType.Int32
          case 1 => IntegerType.Int64
        }
        r <- range.arbitrary
      } yield SchemaF.Integer(r, nt)


      def str: Gen[SchemaF[JsonF.Json]] =
        Arbitrary.arbitrary[StringDescription].map(SchemaF.Str.apply)

      def ref = Gen.alphaStr.map(SchemaF.Ref.apply)
      def nil = Gen.const(SchemaF.Null)
      def bool = Gen.const(SchemaF.Boolean)

      def array: Gen[SchemaF[JsonF.Json]] = for {
        bound <- lengthBound.arbitrary
        uniqueItems <- Gen.oneOf(0, 1).map(_ % 1 == 0)
      } yield SchemaF.Collection(bound, uniqueItems, JsonF.obj("type" -> JsonF.fromString("boolean")))

      Arbitrary {
        Gen.oneOf[SchemaF[JsonF.Json]](
          array,
          choices,
          enum,
          integer,
          number,
          str,
          ref,
          nil,
          bool
        )
      }
    }
  }

  private val choices = Schema.choice(
    NonEmptyList(
      Schema.obj("Super", Map("superName" -> Schema.str(StringDescription.Unbounded))),
      Schema.choice(
        NonEmptyList(
          Schema.obj("Admin", Map("adminName" -> Schema.str(StringDescription.Unbounded))),
          Schema.choice(
            NonEmptyList(
              Schema.obj("Normal", Map("normalName" -> Schema.str(StringDescription.Unbounded))),
              Schema.nil
            )
          )
        )
      )
    )
  )

  override implicit val generatorDrivenConfig: PropertyCheckConfiguration =
    PropertyCheckConfig(minSuccessful = 1000, maxSize = 10)

}
