package jason

import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{Matchers, WordSpec}

class JackonSpec extends WordSpec with Matchers with DomainFormats with GeneratorDrivenPropertyChecks with JasonArbitraryHelpers {

  "jackson" should {
    "provide a json prism which preserves symmetry" in {
      forAll { (json: JsonF.Json) =>
        jackson.jsonPrism.getOption(jackson.jsonPrism.reverseGet(json)).contains(json)
      }
    }
  }

  override implicit val generatorDrivenConfig: PropertyCheckConfiguration =
    PropertyCheckConfig(minSuccessful = 1000, maxSize = 10)

}


