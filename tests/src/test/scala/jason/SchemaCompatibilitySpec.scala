package jason

import matryoshka.data.Fix
import matryoshka.patterns._
import matryoshka.implicits._
import matryoshka._
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{FreeSpec, Matchers, WordSpec}
import SchemaCompatibilityF._

class SchemaCompatibilitySpec extends FreeSpec with Matchers with GeneratorDrivenPropertyChecks with JasonArbitraryHelpers {

  "SchemaCompatibility" - {
    "Numeric" - {
      def fromEither(either: Either[NumberType, IntegerType], range: Range) =
        either match {
          case Left(n) => Schema.number(range, n)
          case Right(n) => Schema.integer(range, n)
        }

      "be compatible when: new range is larger then the old range" in {
        forAll { (oldType: Either[NumberType, IntegerType], oldRange: Range, newType: Either[NumberType, IntegerType], newRange: Range) =>
          whenever(newRange.lower <= oldRange.lower && newRange.upper >= oldRange.upper) {
            compat(fromEither(oldType, oldRange), fromEither(newType, newRange)) shouldBe compatible
          }
        }
      }
      "be incompatible when: new range is smaller then the old range" in {
        forAll { (oldType: Either[NumberType, IntegerType], oldRange: Range, newType: Either[NumberType, IntegerType], newRange: Range) =>
          whenever(oldRange != newRange && newRange.lower >= oldRange.lower && newRange.upper <= oldRange.upper) {
            compat(fromEither(oldType, oldRange), fromEither(newType, newRange)) shouldBe rangeIncompatible(oldRange, newRange)
          }
        }
      }

      //TODO: check if format can contain the numeric bound
    }

    "String" - {

      def notUnbounded(stringDescription: StringDescription): Boolean = stringDescription match {
        case StringDescription.Unbounded => false
        case _ => true
      }

      "be compatible when: going from a strict description to unbounded" in {
        forAll { (oldDesc: StringDescription) =>
          whenever(notUnbounded(oldDesc)) {
            compat(Schema.str(oldDesc), Schema.str(StringDescription.Unbounded)) shouldBe compatible
          }
        }
      }

      "be compatible when: length bound is larger or equal" in {
        forAll { (oldDesc: StringDescription.Length, newDesc: StringDescription.Length) =>
          whenever(partialOrderLengthBound.tryCompare(oldDesc.lengthBound, newDesc.lengthBound).exists(x => x == 0 || x == 1)) {
            compat(Schema.str(oldDesc), Schema.str(newDesc)) shouldBe compatible
          }
        }
      }

      "be compatible when: string type is unchanged" in {
        compat(Schema.str(StringDescription.Type(StringType.Hostname)), Schema.str(StringDescription.Type(StringType.Hostname))) shouldBe
          compatible
      }

      "be compatible when: pattern is unchanged" in {
        compat(Schema.str(StringDescription.Pattern("[A-Z]+")), Schema.str(StringDescription.Pattern("[A-Z]+"))) shouldBe
          compatible
      }

      "be compatible when: unbounded to unbounded" in {
        compat(Schema.str(StringDescription.Unbounded), Schema.str(StringDescription.Unbounded)) shouldBe
          compatible
      }

      "be incompatible when: length bound is smaller" in {
        forAll { (oldDesc: StringDescription.Length, newDesc: StringDescription.Length) =>
          whenever(partialOrderLengthBound.tryCompare(oldDesc.lengthBound, newDesc.lengthBound).contains(-1)) {
            compat(Schema.str(oldDesc), Schema.str(newDesc)) shouldBe lengthBoundIncompatible(oldDesc.lengthBound, newDesc.lengthBound)
          }
        }
      }


      "be incompatible when: going from unbounded to strict" in {
        forAll { (newDesc: StringDescription) =>
          whenever(notUnbounded(newDesc)) {
            compat(Schema.str(StringDescription.Unbounded), Schema.str(newDesc)) shouldBe stringWidening(newDesc)
          }
        }
      }

      "be incompatible when: string type is changed" in {
        forAll { (oldDesc: StringDescription.Type, newDesc: StringDescription.Type) =>
          whenever(oldDesc != newDesc) {
            compat(Schema.str(oldDesc), Schema.str(newDesc)) shouldBe
              stringTypeIncompatible(oldDesc.stringType, newDesc.stringType)
          }
        }
      }

      "be incompatible when: potential pattern discrepancy" in {
        forAll { (oldDesc: StringDescription.Pattern, newDesc: StringDescription.Pattern) => 
          whenever(oldDesc != newDesc) {
            compat(Schema.str(oldDesc), Schema.str(newDesc)) shouldBe
              potentialPatternDiscrepancy(oldDesc.pattern, newDesc.pattern)
          }
        }
      }
    }

    "Ref" - {
      val personSchema = Map(
        "name" -> Schema.str(StringDescription.Unbounded),
        "age" -> Schema.integer(Range(Bound(Int.MinValue, true), Bound(Int.MaxValue, true)), IntegerType.Int32)
      )

      val oldManifest = SchemaManifest.single("Person" -> SchemaManifestEntry(personSchema, Set("name", "age")))
      val newManifest = SchemaManifest.single("Persona" -> SchemaManifestEntry(personSchema, Set("name", "age")))

      "be compatible when: refs are the same" in {
        compat(Schema.ref("Person"), Schema.ref("Person"), oldManifest, oldManifest) shouldBe compatible
      }
      "be compatible when: ref becomes optional, but is still the same ref" in {
        compat(Schema.ref("Person"), Schema.opt(Schema.ref("Person")), oldManifest, oldManifest) shouldBe objectCompatibility(Map("name" -> compatible, "age" -> compatible))
      }
      "be compatible when: ref changes, but both type shapes are compatible" in {
        compat(Schema.ref("Person"), Schema.opt(Schema.ref("Persona")), oldManifest, newManifest) shouldBe objectCompatibility(Map("name" -> compatible, "age" -> compatible))
      }
      "be incompatible when: ref changes, but type shapes are in compatible" in {
        compat(Schema.ref("Person"), Schema.opt(Schema.ref("Persona")), oldManifest, SchemaManifest.empty) shouldBe refIncompatible("Person", "Persona")
      }
    }

    "Collection" - {
      "be compatible when: unchanged" in {
        compat(
          Schema.collection(LengthBound.Unbounded, true, Schema.bool),
          Schema.collection(LengthBound.Unbounded, true, Schema.bool)) shouldBe compatible
      }
      "be compatible when: length bound is larger or equal" in {
        forAll { (oldBound: LengthBound, newBound: LengthBound) =>
          whenever(partialOrderLengthBound.tryCompare(oldBound, newBound).exists(x => x == 0 || x == 1)) {
            compat(
              Schema.collection(oldBound, true, Schema.bool),
              Schema.collection(newBound, true, Schema.bool)
            ) shouldBe compatible
          }
        }
      }

      "be compatible when: uniqueItems changed from true -> false" in {
        compat(
          Schema.collection(LengthBound.Unbounded, true, Schema.bool),
          Schema.collection(LengthBound.Unbounded, false, Schema.bool)) shouldBe compatible
      }

      "be incompatible when: length bound is smaller" in {
        forAll { (oldBound: LengthBound, newBound: LengthBound) =>
          whenever(partialOrderLengthBound.tryCompare(oldBound, newBound).contains(-1)) {
            compat(
              Schema.collection(oldBound, true, Schema.bool),
              Schema.collection(newBound, true, Schema.bool)
            ) shouldBe lengthBoundIncompatible(oldBound, newBound)
          }
        }
      }

      "be incompatible when: uniqueItems changed from false -> true" in {
        compat(
          Schema.collection(LengthBound.Unbounded, false, Schema.bool),
          Schema.collection(LengthBound.Unbounded, true, Schema.bool)) shouldBe cannotGuaranteeUniquenessInCollection
      }
    }
  }

  private val partialOrderLengthBound = implicitly[PartialOrdering[LengthBound]]

  private def compat(a: Fix[SchemaF], b: Fix[SchemaF]): SchemaCompatibility =
    compat(a, b, SchemaManifest.empty, SchemaManifest.empty)

  private def compat(oldSchema: Fix[SchemaF], newSchema: Fix[SchemaF], oldManifest: SchemaManifest, newManifest: SchemaManifest): SchemaCompatibility = {
    val d: Fix[Diff[Fix, SchemaF, ?]] = oldSchema.paraMerga(newSchema)(patterns.diff)
    d.cata(SchemaCompatibilityChecker.algebra(oldManifest, newManifest))
  }

}
