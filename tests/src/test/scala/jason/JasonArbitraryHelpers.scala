package jason

import org.scalacheck.Gen.Choose
import org.scalacheck.{Arbitrary, Gen}
import org.scalatest.{Matchers, WordSpecLike}

import scalaz._
import scalaz.Scalaz._

trait JasonArbitraryHelpers {

  implicit val monad: Monad[Gen] = new Monad[Gen] {
    override def point[A](a: => A): Gen[A] = Gen.const(a)
    override def bind[A, B](fa: Gen[A])(f: A => Gen[B]): Gen[B] = fa.flatMap(f)
  }

  private def genJson: Gen[JsonF.Json] =
    Gen.oneOf(
      str,
      bool,
      array,
      obj,
      number,
      nil
    )

  private def str = Gen.alphaStr.map(JsonF.fromString)
  private def bool = Gen.oneOf(0, 1).map(_ % 1 == 0).map(JsonF.fromBoolean)
  private def array = Gen.listOf(Gen.const(JsonF.fromBoolean(true))).map(JsonF.fromValues)
  private def obj = Gen.mapOf {
    for {
      key <- Gen.alphaStr
      value <- Gen.const(JsonF.fromBoolean(true))
    } yield key -> value
  }.map(JsonF.fromFields)

  private def number = Arbitrary.arbitrary[BigDecimal].map(JsonF.fromBigDecimal)
  private def nil = Gen.const(JsonF.nil)

  implicit val arbJson: Arbitrary[JsonF.Json] = Arbitrary(genJson)

  implicit def lengthBound: Arbitrary[LengthBound] = Arbitrary {
    Gen.oneOf(
      Gen.const(LengthBound.Unbounded),
      Gen.chooseNum(Int.MinValue, Int.MaxValue).map(LengthBound.Atleast.apply),
      Gen.chooseNum(Int.MinValue, Int.MaxValue).map(LengthBound.Atmost.apply),
      (Gen.chooseNum(Int.MinValue, 0) |@| Gen.chooseNum(1, Int.MaxValue))(LengthBound.Interval.apply)
    )
  }

  implicit def numberType: Arbitrary[NumberType] = Arbitrary(Gen.oneOf(NumberType.Float, NumberType.Double))
  implicit def integerType: Arbitrary[IntegerType] = Arbitrary(Gen.oneOf(IntegerType.Int32, IntegerType.Int64))

  implicit def stringDescriptionType: Arbitrary[StringDescription.Type] =
    Arbitrary(Gen.oneOf(StringType.Email, StringType.IPV6, StringType.IPV4, StringType.DateTime, StringType.Hostname, StringType.Uri).map(StringDescription.Type.apply))

  implicit def stringDescriptionLength: Arbitrary[StringDescription.Length] =
    Arbitrary(lengthBound.arbitrary.map(StringDescription.Length.apply))

  implicit def stringDescriptionPattern: Arbitrary[StringDescription.Pattern] =
    Arbitrary(Gen.alphaStr.map(StringDescription.Pattern.apply))

  implicit def stringDescription: Arbitrary[StringDescription] =
    Arbitrary(
      Gen.oneOf(
        Arbitrary.arbitrary[StringDescription.Type],
        Arbitrary.arbitrary[StringDescription.Length],
        Arbitrary.arbitrary[StringDescription.Pattern],
        Gen.const(StringDescription.Unbounded)
      )
    )

  implicit def range: Arbitrary[Range] = Arbitrary{
    def bound(v: BigDecimal): Gen[Bound] = for {
      inclusive <- Gen.oneOf(0, 1).map(_ % 1 == 0)
    } yield Bound(v, inclusive)

    for {
      min <- Gen.chooseNum(Long.MinValue, 0l)
      max <- Gen.chooseNum(1l, Long.MaxValue)
      lower <- bound(min)
      upper <- bound(max)
    } yield Range(lower, upper)
  }
}
