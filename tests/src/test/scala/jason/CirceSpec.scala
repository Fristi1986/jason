package jason

import io.circe.{Decoder, Encoder}
import jason.circe._
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{Matchers, WordSpec}

class CirceSpec extends WordSpec with Matchers with DomainFormats with GeneratorDrivenPropertyChecks with JasonArbitraryHelpers {

  "circe" should {
    "provide encoders/decoders which preserve symmetry" in {
      forAll { (a: AllTypes) =>
        Decoder[AllTypes].decodeJson(Encoder[AllTypes].apply(a)) shouldBe Right(a)
      }
    }
    "provide a json prism which preserves symmetry" in {
      forAll { (json: JsonF.Json) =>
        circe.jsonPrism.getOption(circe.jsonPrism.reverseGet(json)).contains(json)
      }
    }
  }

  override implicit val generatorDrivenConfig: PropertyCheckConfiguration =
    PropertyCheckConfig(minSuccessful = 1000, maxSize = 10)

}


