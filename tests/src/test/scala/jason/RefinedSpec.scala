package jason

import eu.timepit.refined.api.Refined
import eu.timepit.refined.string._
import eu.timepit.refined._
import eu.timepit.refined.numeric._
import org.scalatest.{Matchers, WordSpec}
import jason.refined._

class RefinedSpec extends WordSpec with Matchers {
  import Schema._

  "Refined" should {
    "String" should {
      "MatchesRegex" in {
        implicitly[JasonSchema[RefinedClass]].create shouldBe obj("RefinedClass",
          Map(
            "name" -> str(StringDescription.Pattern("""[A-Za-z+]""")),
            "uri" -> str(StringDescription.Type(StringType.Uri)),
            "age" -> integer(Range(Bound(1, true), Bound(Int.MaxValue, true)), IntegerType.Int32),
            "hue" -> integer(Range(Bound(1, true), Bound(10, true)), IntegerType.Int32)
          )
        )
      }
    }
  }

  implicit val refinedClass: Jason[RefinedClass] = object4("RefinedClass")(RefinedClass.apply)(
    "name" -> member(string.matchesRegex, _.name),
    "uri" -> member(string.url, _.uri),
    "age" -> member(int.positive, _.age),
    "hue" -> member(int.intervalOpen, _.hue)
  )

}

case class RefinedClass(
  name: String Refined MatchesRegex[W.`"[A-Za-z+]"`.T],
  uri: String Refined Url,
  age: Int Refined Positive,
  hue: Int Refined Interval.Open[W.`1`.T, W.`10`.T]
)
