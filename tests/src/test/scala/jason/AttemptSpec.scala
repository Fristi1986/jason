package jason

import org.scalatest.{Matchers, WordSpec}

import scala.util.Try

class AttemptSpec extends WordSpec with Matchers {

  "Attempt" should {
    "toEither" should {
      "Right(1)" in {
        Attempt.success(1).toEither shouldBe Right(1)
      }
      "Left(err) with Throwable" in {
        Attempt.exception(new Throwable("err")).toEither shouldBe Left("err")
      }
      "Left(err) with Error" in {
        Attempt.error("err").toEither shouldBe Left("err")
      }
    }
    "fromTry" should {
      "Success" in {
        Attempt.fromTry(Try(1)) shouldBe Attempt.success(1)
      }
      "Failure" in {
        Attempt.fromTry(scala.util.Failure(new Throwable("err"))) shouldBe Attempt.exception(new Throwable("err"))
      }
    }
    "fromOption" should {
      "Some" in {
        Attempt.fromOption(Some(1), "err") shouldBe Attempt.success(1)
      }
      "None" in {
        Attempt.fromOption(None, "err") shouldBe Attempt.error("err")
      }
    }
    "fromEither" should {
      "Some" in {
        Attempt.fromEither(Right(1)) shouldBe Attempt.success(1)
      }
      "None" in {
        Attempt.fromEither(Left("err")) shouldBe Attempt.error("err")
      }
    }
  }

}
