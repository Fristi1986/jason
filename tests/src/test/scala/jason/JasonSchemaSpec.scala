package jason

import java.time.{Instant, LocalDate, LocalDateTime, ZonedDateTime}
import java.util.UUID

import matryoshka.data.Fix
import org.scalatest.{Matchers, WordSpec}

import scalaz.NonEmptyList

class JasonSchemaSpec extends WordSpec with Matchers with DomainFormats {
  "JasonSchema.create" should {
    "emit a correct tree for product type" in {
      JasonSchema[AllTypes].create shouldBe Schema.obj("AllTypes",
        Map(
          "bool" -> Schema.bool,
          "string" -> Schema.str(StringDescription.Unbounded),
          "int" -> Schema.integer(Range(Bound(Int.MinValue, true), Bound(Int.MaxValue, true)), IntegerType.Int32),
          "long" -> Schema.integer(Range(Bound(Long.MinValue, true), Bound(Long.MaxValue, true)), IntegerType.Int64),
          "float" -> Schema.number(Range(Bound(BigDecimal(Float.MinValue), true), Bound(BigDecimal(Float.MaxValue), true)), NumberType.Float),
          "double" -> Schema.number(Range(Bound(Double.MinValue, true), Bound(Double.MaxValue, true)), NumberType.Double),
          "uuid" -> Schema.str(StringDescription.Pattern("/^[A-F\\d]{8}-[A-F\\d]{4}-4[A-F\\d]{3}-[89AB][A-F\\d]{3}-[A-F\\d]{12}$/")),
          "instant" -> Schema.integer(Range(Bound(0, true), Bound(Long.MaxValue, true)), IntegerType.Int64),
          "localDate" -> Schema.str(StringDescription.Unbounded),
          "localDateTime" -> Schema.str(StringDescription.Unbounded),
          "zonedDateTime" -> Schema.str(StringDescription.Unbounded),
          "list" -> Schema.collection(LengthBound.Atleast(0), false, Schema.integer(Range(Bound(Long.MinValue, true), Bound(Long.MaxValue, true)), IntegerType.Int64)),
          "set" -> Schema.collection(LengthBound.Atleast(0), true, Schema.integer(Range(Bound(Long.MinValue, true), Bound(Long.MaxValue, true)), IntegerType.Int64)),
          "vector" -> Schema.collection(LengthBound.Atleast(0), false, Schema.integer(Range(Bound(Long.MinValue, true), Bound(Long.MaxValue, true)), IntegerType.Int64)),
          "option" -> Schema.opt(Schema.integer(Range(Bound(Long.MinValue, true), Bound(Long.MaxValue, true)), IntegerType.Int64)),
          "seq" -> Schema.collection(LengthBound.Atleast(0), false, Schema.integer(Range(Bound(Long.MinValue, true), Bound(Long.MaxValue, true)), IntegerType.Int64)),
          "either" -> Schema.choice(NonEmptyList(Schema.str(StringDescription.Unbounded), Schema.integer(Range(Bound(Long.MinValue, true), Bound(Long.MaxValue, true)), IntegerType.Int64)))
        )
      )
    }
    "emit a correct tree for a coproduct type" in {
      JasonSchema[User].create shouldBe Schema.choice(
        NonEmptyList(
          Schema.obj("Super", Map("superName" -> Schema.str(StringDescription.Unbounded))),
          Schema.choice(
            NonEmptyList(
              Schema.obj("Admin", Map("adminName" -> Schema.str(StringDescription.Unbounded))),
              Schema.choice(
                NonEmptyList(
                  Schema.obj("Normal", Map("normalName" -> Schema.str(StringDescription.Unbounded))),
                  Schema.nil
                )
              )
            )
          )
        )
      )
    }
  }
}

